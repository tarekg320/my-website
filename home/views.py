import  logging
import datetime

from django.shortcuts import render,redirect
from django.urls import reverse

from .models import Article,ProgrammingLanguage,Framework,Library
from django.utils import translation

# Create your views here.

def home(request):
    context={'pageName':'home'}
    response = render(request,'home/home.html',context=context) 
    cheek_cookies(request,response)
    # translation.activate('ar')
    # return render(request,'home/home.html',context=context)
    return  response



def courses(request):
    # show = request.COOKIES['dataflair']
    # logging.warning(show)
    libraries = Library.objects.all()
    languages = ProgrammingLanguage.objects.all()
    framework = Framework.objects.all()

    context = {'libraries':libraries,'languages':languages,'framework':framework}
    response  = render(request,'home/courses.html',context=context)
    cheek_cookies(request,response)
    return response

def About(request):
    context={'pageName':'about'}
    response = render(request,'home/about.html',context=context)
    cheek_cookies(request,response)
    return response



def change_languge(request , lang:str):
    response = redirect(home)
    
    if(lang in ('ar','en')):
        set_cookies(request, response,lang)
    return response





def cheek_cookies(request,response):
    try:
        show1 = request.COOKIES['language']
        # show2 = request.COOKIES['cookies_enable']
        if show1 in ('ar','en') :
            translation.activate(show1)
        
    except Exception as identifier:
        set_cookies(request,response,'en')
        logging.warning(identifier)
        logging.warning('cheek_cookies')






def set_cookies(request,response,lang='en'):
    try:
        translation.activate(lang)
        response.set_cookie('language', lang, max_age = None)
        # response.set_cookie('cookies_enable', 'yes', max_age =none)
    except Exception as identifier:
        logging.warning(identifier)
        logging.warning('set_cookies')