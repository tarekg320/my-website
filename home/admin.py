from django.contrib import admin

from .models import Article,Tag,ProgrammingLanguage,Framework,Library

admin.site.register(Article)
admin.site.register(Tag)
admin.site.register(ProgrammingLanguage)
admin.site.register(Framework)
admin.site.register(Library)