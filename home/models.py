from django.db import models

class Article(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return f"{self.question_text} y={self.pub_date.year} m={self.pub_date.month} d={self.pub_date.day}"

    models.ManyToManyField



class Tag(models.Model):
    tag_name = models.CharField(max_length=200)

    def __str__(self):
        return self.tag_name


class ProgrammingLanguage(models.Model):
    tags = models.ManyToManyField('Tag')
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
class Framework(models.Model):
    tags = models.ManyToManyField('Tag')
    name = models.CharField(max_length=200)



class Library(models.Model):
    tags = models.ManyToManyField('Tag')
    name = models.CharField(max_length=200)
    def __str__(self):
       return self.name
# class Framework(models.Model):
#     pass