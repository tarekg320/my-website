from django.urls import path
from .views import home  ,About,courses,change_languge

urlpatterns = [
    path('', home, name='home'),
    path('languge/<str:lang>', change_languge, name='action'),
    path('courses', courses, name='courses'),
    path('about', About, name='about'),
]